README
======
GNSS receiver for Raspberry Pi like boards using Buildroot version 2024.02.

User login
----------
User can log in via serial port, where is getty listening. The default credentials are following:

* user: ``abeona``
* password: ``Ch@ngePwd!``

**Please change the password** - to do that, run ``passwd`` utility (available in the image).

WiFi network
------------
On Raspberry Zero W will be WiFi enabled on start. It will try to connect to the following network:

* ssid: ``gnss``
* password: ``Meich3YaT0re``

To change default network settings, simply edit wpa configuration, like this:
``vi /etc/wpa_supplicant.conf``.
