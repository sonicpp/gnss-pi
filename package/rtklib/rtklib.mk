################################################################################
#
# rtklib
#
################################################################################

RTKLIB_VERSION = b34e
RTKLIB_SITE = $(call github,rtklibexplorer,RTKLIB,$(RTKLIB_VERSION))
RTKLIB_LICENSE = BSD-2-Clause
RTKLIB_LICENSE_FILES = license.txt

RTKLIB_APPS += $(if $(BR2_PACKAGE_RTKLIB_CONVBIN),convbin,)
RTKLIB_APPS += $(if $(BR2_PACKAGE_RTKLIB_POS2KML),pos2kml,)
RTKLIB_APPS += $(if $(BR2_PACKAGE_RTKLIB_RNX2RTKP),rnx2rtkp,)
RTKLIB_APPS += $(if $(BR2_PACKAGE_RTKLIB_RTKRCV),rtkrcv,)
RTKLIB_APPS += $(if $(BR2_PACKAGE_RTKLIB_STR2STR),str2str,)

define RTKLIB_BUILD_CMDS
	for APP in $(RTKLIB_APPS); do \
		cd $(@D)/app/consapp/$$APP/gcc ; \
		$(TARGET_MAKE_ENV) $(MAKE) CC="$(TARGET_CC)" CCFLAGS="$(TARGET_CFLAGS) $(TARGET_LDFLAGS)" ; \
	done
endef

define RTKLIB_INSTALL_TARGET_CMDS
	for APP in $(RTKLIB_APPS); do \
		$(INSTALL) -m 0755 $(@D)/app/consapp/$$APP/gcc/$$APP $(TARGET_DIR)/usr/bin/$$APP ; \
	done
endef

$(eval $(generic-package))
