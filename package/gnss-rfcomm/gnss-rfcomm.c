#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

#define _POSIX_SOURCE	1 /* POSIX compliant source */

#define BAUDRATE	B115200
#define MODEMDEVICE	"/dev/ttyACM0"
#define BUF_SIZE	256

static int term_open(const char *path)
{
	int fd_term = open(path, O_RDONLY | O_NOCTTY);

	if (fd_term == -1) {
		perror("open() failed");
		return -1;
	}

	return fd_term;
}

static int term_prepare(int fd_term, struct termios *oldtio)
{
	struct termios newtio;

	/* Store old term attributes */
	if (tcgetattr(fd_term, oldtio) == -1) {
		perror("tcgetattr() failed");
		return -1;
	}

	/* Prepare new term attributes */
	memset(&newtio, 0, sizeof(newtio));
	newtio.c_iflag = IGNPAR;
	newtio.c_cflag = CS8 | CREAD | PARENB | PARODD | CLOCAL;
	newtio.c_cc[VMIN] = 1;

	if (cfsetispeed(&newtio, BAUDRATE) == -1) {
		perror("cfsetispeed() failed");
		return -1;
	}

	/* Load new term attributes */
	tcflush(fd_term, TCIFLUSH);
	if (tcsetattr(fd_term, TCSANOW, &newtio) == -1) {
		perror("tcsetattr() failed");
		return -1;
	}

	return 0;
}

static int bt_prepare_server()
{
	struct sockaddr_rc loc_addr = { 0 };
	int sckt_server;

	sckt_server = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
	if (sckt_server == -1) {
		perror("socket() failed");
		return -1;
	}

	loc_addr.rc_family = AF_BLUETOOTH;
	loc_addr.rc_bdaddr = *BDADDR_ANY;
	loc_addr.rc_channel = 0;
	if (bind(sckt_server, (struct sockaddr *) &loc_addr, sizeof(loc_addr)) == -1) {
		perror("bind() failed");
		close(sckt_server);
		return -1;
	}

	printf("Entering listen mode\n");
	if (listen(sckt_server, 1) == -1) {
		perror("listen() failed");
		close(sckt_server);
		return -1;
	}

	return sckt_server;
}

static int bt_accept_client(int sckt_server)
{
	struct sockaddr_rc rem_addr = { 0 };
	char addr[32] = { 0 };
	int sckt_client;
	socklen_t opt = sizeof(rem_addr);

	printf("Entering accept mode\n");
	sckt_client = accept(sckt_server, (struct sockaddr *) &rem_addr, &opt);
	if (sckt_client == -1) {
		perror("accept() failed");
		return -1;
	}

	ba2str(&rem_addr.rc_bdaddr, addr);
	printf("Accepted client: %s\n", addr);

	return sckt_client;
}

int main(int argc, char **argv)
{
	int sckt_server, sckt_client;
	int bytes_read;
	int fd_term;
	char buf[BUF_SIZE];
	struct termios oldtio;
	int ret = 1;
	int done = 0;

	fd_term = term_open(MODEMDEVICE);
	if (fd_term == -1)
		return 1;

	if (term_prepare(fd_term, &oldtio) == -1)
		goto err_term;

	sckt_server = bt_prepare_server();
	if (sckt_server == -1)
		goto err_server;

	sckt_client = bt_accept_client(sckt_server);
	if (sckt_client == -1)
		goto err_client;

	while (!done) {
		if ((bytes_read = read(fd_term, buf, BUF_SIZE)) == -1) {
			perror("GNSS read error");
			done = 1;
		} else if (write(sckt_client, buf, bytes_read) == -1) {
			perror("BT write error");
			done = 1;
		}
	}

	ret = 0;

	close(sckt_client);
err_client:
	close(sckt_server);
err_server:
	tcsetattr(fd_term, TCSANOW, &oldtio);
err_term:
	close(fd_term);

	return ret;
}
