################################################################################
#
# gnss-rfcomm
#
################################################################################

GNSS_RFCOMM_DEPENDENCIES = bluez5_utils

define GNSS_RFCOMM_BUILD_CMDS
	$(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) $(GNSS_RFCOMM_PKGDIR)/gnss-rfcomm.c -lbluetooth -o $(@D)/gnss-rfcomm
endef

define GNSS_RFCOMM_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 $(@D)/gnss-rfcomm $(TARGET_DIR)/usr/bin/gnss-rfcomm
endef

define GNSS_RFCOMM_INSTALL_INIT_SYSV
	$(INSTALL) -m 755 -D $(GNSS_RFCOMM_PKGDIR)/S70gnss-rfcomm \
		$(TARGET_DIR)/etc/init.d/
endef

$(eval $(generic-package))
